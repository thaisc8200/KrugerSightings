# Desenvolupadors:
- Thais Camuñez
- Esteban Garcia

# Items desenvolupats:
- Realitzar imatges 
- Guardar les imatges en local i al núvol
- Mostrar imatges
- Mostrar ubicacions en un mapa
- Filtrar les imatges per animal i tenir una galeria per cada un
- Mostrar la ubicació d'una imatge al clicar-la
- Diferenciar els marcadors per color segons animal

# Trello
https://trello.com/b/8701rXWl/kruger

# Disseny Figma
https://www.figma.com/file/UcszpGmdOVO9Ug5gR5lr4b/Material-Baseline-Design-Kit?node-id=32%3A6099