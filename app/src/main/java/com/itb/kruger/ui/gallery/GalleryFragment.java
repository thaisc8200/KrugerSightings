package com.itb.kruger.ui.gallery;

import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.ListResult;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.itb.kruger.R;
import com.itb.kruger.model.Photo;

import java.util.ArrayList;
import java.util.List;

public class GalleryFragment extends Fragment {

    private GalleryViewModel galleryViewModel;
    private RecyclerView recyclerView;
    private GalleryAdapter adapter;
    private LiveData<List<Photo>> photos;
    private String animal;
    private ProgressDialog pd;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_gallery, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = view.findViewById(R.id.rvGallery);
        animal = GalleryFragmentArgs.fromBundle(getArguments()).getAnimal();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        galleryViewModel = ViewModelProviders.of(this).get(GalleryViewModel.class);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(),3);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new GalleryAdapter();
        recyclerView.setAdapter(adapter);
        adapter.setOnPhotoClickListener(this::viewPhoto);

        pd = new ProgressDialog(getContext());

        if(!animal.equals("gallery")){
            photos = galleryViewModel.getPhotosByAnimal(animal,pd);
        } else {
            photos = galleryViewModel.getAllPhotos(pd);
        }
        photos.observe(getViewLifecycleOwner(), this::onGalleryChanged);
    }

    private void onGalleryChanged(List<Photo> photos) {
        adapter.setPhotos(photos);
    }

    private void viewPhoto(Photo photo) {
        Bundle bundle = new Bundle();
        bundle.putDouble("Latitude", photo.getLatitude());
        bundle.putDouble("Longitude", photo.getLongitude());
        Navigation.findNavController(getView()).navigate(R.id.GalleryToHome, bundle);
    }
}