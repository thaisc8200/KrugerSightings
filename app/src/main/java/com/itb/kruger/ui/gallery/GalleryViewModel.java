package com.itb.kruger.ui.gallery;

import android.app.Application;
import android.app.ProgressDialog;

import com.itb.kruger.model.Photo;
import com.itb.kruger.repository.Repository;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class GalleryViewModel extends AndroidViewModel {
    Repository repository;

    public GalleryViewModel(@NonNull Application application) {
        super(application);
        repository = new Repository(application);
    }

    public LiveData<List<Photo>> getAllPhotos(ProgressDialog pd){
        return repository.getAllPhotos(pd);
    }

    public LiveData<List<Photo>> getPhotosByAnimal(String animal, ProgressDialog pd){
        return repository.getPhotosByAnimal(animal, pd);
    }
}