package com.itb.kruger.ui.camera;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.itb.kruger.R;

import static android.app.Activity.RESULT_OK;

public class CameraFragment extends Fragment {
    private static final int REQUEST_IMAGE_CAPTURE = 1;
    private static final int REQUEST_TAKE_PHOTO = 1;

    private CameraViewModel cameraViewModel;

    private FloatingActionButton btnTakePictures;
    private FloatingActionButton btnSavePictures;
    private ImageView ivPreview;
    private Spinner spinnerAnimal;
    private ProgressDialog pd;

    private Location location;


    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        cameraViewModel = ViewModelProviders.of(this).get(CameraViewModel.class);
        return inflater.inflate(R.layout.fragment_camera, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ivPreview = view.findViewById(R.id.ivPreview);
        spinnerAnimal = view.findViewById(R.id.spinnerAnimal);
        btnTakePictures = view.findViewById(R.id.btnTakePicture);
        btnSavePictures = view.findViewById(R.id.btnSavePicture);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        btnTakePictures.setOnClickListener(this::onPhotoButtonClicked);
        pd = new ProgressDialog(getContext());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        Glide.with(getContext()).load(cameraViewModel.getPhotoPath()).into(ivPreview);

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            btnSavePictures.setOnClickListener(this::OnSaveButtonClicked);
        }
    }

    private void OnSaveButtonClicked(View view) {
       cameraViewModel.putFileWithMetadata(location,spinnerAnimal,getContext(),pd);
    }

    private void onPhotoButtonClicked(View view) throws SecurityException {
        LocationManager lm = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if(location != null){
            cameraViewModel.dipatchPhoto(this,REQUEST_TAKE_PHOTO);
        } else {
            location = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            cameraViewModel.dipatchPhoto(this,REQUEST_TAKE_PHOTO);
        }

    }
}